import { instantiate } from "../lib/rs_lib.generated.js";
import { assertEquals } from "https://deno.land/std@0.159.0/testing/asserts.ts";
const { render_rst } = await instantiate();

const RST = `* this is
* a list

  * with a nested list
  * and some subitems

* and here the parent list continues

| aaa
| bbb
|

aaa
============================

bbbccc
`;

const HTML = `<!doctype html><html>
<ul>
<li><p>this is</p></li>
<li><p>a list</p><ul>
<li><p>with a nested list</p></li>
<li><p>and some subitems</p></li>
</ul></li>
<li><p>and here the parent list continues</p></li>
</ul>
</html>
`;

Deno.test({
  name: "rst rendering test",
  fn: () => {
    const ret = render_rst(RST, true)
    assertEquals(ret, HTML);
  },
});
