use std::io::{self, Write};

#[derive(Debug, Clone)]
enum Token {
    Number(f64),
    Op(char),
    LeftParen,
    RightParen,
}

#[derive(Debug)]
enum Expr {
    Number(f64),
    BinOp(char, Box<Expr>, Box<Expr>),
}

fn main() {
    loop {
        let mut input = String::new();
        print!("> ");
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut input).unwrap();
        let input = input.trim();
        if input == "quit" {
            break;
        }
        let tokens = tokenize(input);
        let ast = parse(&tokens);
        let expr = parse_expr(&ast);
        let result = evaluate(&expr);
        println!("{}", result);
    }
}


fn tokenize(input: &str) -> Vec<Token> {
    let mut tokens = Vec::new();
    let mut chars = input.chars().peekable();
    while let Some(ch) = chars.next() {
        match ch {
            '0'..='9' => {
                let mut num_str = String::new();
                num_str.push(ch);
                while let Some('0'..='9' | '.') = chars.peek().cloned() {
                    num_str.push(chars.next().unwrap());
                }
                let num = num_str.parse().unwrap();
                tokens.push(Token::Number(num));
            }
            '+' | '-' | '*' | '/' => {
                tokens.push(Token::Op(ch));
            }
            '(' => {
                tokens.push(Token::LeftParen);
            }
            ')' => {
                tokens.push(Token::RightParen);
            }
            ' ' => {
                // Ignore whitespace
            }
            _ => {
                panic!("Invalid character: {}", ch);
            }
        }
    }
    tokens
}

fn parse(tokens: &[Token]) -> Vec<Token> {
    let mut output: Vec<Token> = vec![];
    let mut stack: Vec<Token> = vec![];
    for token in tokens {
        match *token {
            Token::Number(num) => output.push(Token::Number(num)),
            Token::Op(op) => {
                while let Some(Token::Op(top_op)) = stack.last().cloned() {
                    if top_op == '(' || precedence(top_op) < precedence(op) {
                        break;
                    }
                    output.push(stack.pop().unwrap());
                }
                stack.push(Token::Op(op));
            }
            Token::LeftParen => {
                stack.push(Token::LeftParen);
            }
            Token::RightParen => {
                while let Some(token) = stack.pop() {
                    match token {
                        Token::LeftParen => break,
                        _ => output.push(token),
                    }
                }
            }
        }
    }
    while let Some(token) = stack.pop() {
        output.push(token);
    }
    output
}

fn precedence(op: char) -> i32 {
    match op {
        '+' | '-' => 1,
        '*' | '/' => 2,
        _ => 0,
    }
}

fn parse_expr(tokens: &[Token]) -> Expr {
    let mut stack: Vec<Expr> = vec![];
    for token in tokens {
        match *token {
            Token::Number(number) => {
                stack.push(Expr::Number(number));
            }
            Token::Op(op) => {
                let right = stack.pop().unwrap();
                let left = stack.pop().unwrap();
                stack.push(Expr::BinOp(op, Box::new(left), Box::new(right)));
            }
            _ => panic!("Invalid token"),
        }
    }
    assert_eq!(stack.len(), 1);
    stack.pop().unwrap()
}

fn evaluate(expr: &Expr) -> f64 {
    match expr {
        Expr::Number(number) => *number,
        Expr::BinOp('+', ref left, ref right) => evaluate(left) + evaluate(right),
        Expr::BinOp('-', ref left, ref right) => evaluate(left) - evaluate(right),
        Expr::BinOp('*', ref left, ref right) => evaluate(left) * evaluate(right),
        Expr::BinOp('/', ref left, ref right) => evaluate(left) / evaluate(right),
        _ => panic!("Invalid expression"),
    }
}
